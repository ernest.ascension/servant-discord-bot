servant discord bot
======================
A simple discord bot that serves your purposes.

# Deploying the bot

Bot invitation link:

https://discord.com/oauth2/authorize?client_id=743119257264717914&scope=bot&permissions=8

# Development

## Requirements

- node v12+
- npm

## Steps

1. Clone the repository into your local machine ```git clone <link>```.
2. Create a discord application.
3. Create a discord bot.
4. rename `config.json.example` into `config.json`, then paste your token into the token field inside this file.
5. Customize the link in accordance to your client id (from app) and permission (can be found in the bot menu).
6. run ```npm install```.
7. run ```node .```.
8. Open the customized link in the browser.

You're good to go! The bot should be up and running.

### Credits to erunesu-nyan