module.exports = {
	name: 'target',
	description: 'Order the servant to spy on someone!',
	execute(message, args) {
        if (!message.mentions.users.size) {
            return message.reply('Pardon me, who do I need to target again?');
        }
        const taggedUser = message.mentions.users.first();
        message.reply(`I will keep a close eye on ${taggedUser.username}`);
    },
};