module.exports = {
	name: 'whoami',
	description: 'Show your user profile',
	execute(message, args) {
        message.reply(`Your username: ${message.author.username}\nYour ID: ${message.author.id}`);
    },
};