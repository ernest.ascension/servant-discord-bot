module.exports = {
	name: 'server',
	description: 'Show server profile',
	execute(message, args) {
        message.reply(`Server name: ${message.guild.name}\nTotal members: ${message.guild.memberCount}`);
    },
};